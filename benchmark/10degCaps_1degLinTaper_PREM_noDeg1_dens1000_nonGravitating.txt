Station  Lat(+N,deg)  Lon(+E,deg)  E-Amp(mm)  E-Pha(deg)  N-Amp(mm)  N-Pha(deg)  V-Amp(mm)  V-Pha(deg) 
T001a_      89.99      0.0      0.0      0.0      0.0011751      0.0      51.13565413      180.0
T001b_      89.0      0.0      0.0      180.0      0.12109765      0.0      51.01319722      180.0
T001c_      88.0      0.0      0.0      180.0      0.25962724      0.0      50.69170405      180.0
T001d_      87.0      0.0      0.0      180.0      0.42542167      0.0      50.12088345      180.0
T001e_      86.0      0.0      0.0      180.0      0.6419881      0.0      49.26569814      180.0
T001f_      85.0      0.0      0.0      180.0      0.93439901      0.0      48.05580338      180.0
T001g_      84.0      0.0      0.0      180.0      1.34635839      0.0      46.36531857      180.0
T001h_      83.0      0.0      0.0      180.0      1.93960567      0.0      43.96385835      180.0
T001i_      82.0      0.0      0.0      180.0      2.81548527      0.0      40.36640718      180.0
T001j_      81.0      0.0      0.0      180.0      4.10714024      0.0      34.11818436      180.0
T001k_      80.9      0.0      0.0      180.0      4.24662549      0.0      33.00763662      180.0
T001l_      80.8      0.0      0.0      180.0      4.34341227      0.0      31.78490866      180.0
T001m_      80.7      0.0      0.0      180.0      4.40390437      0.0      30.51089519      180.0
T001n_      80.6      0.0      0.0      180.0      4.42997827      0.0      29.20882048      180.0
T001o_      80.5      0.0      0.0      180.0      4.42189353      0.0      27.89517962      180.0
T001p_      80.4      0.0      0.0      180.0      4.37982567      0.0      26.58517073      180.0
T001q_      80.3      0.0      0.0      180.0      4.30424161      0.0      25.29376817      180.0
T001r_      80.2      0.0      0.0      180.0      4.19552968      0.0      24.03711435      180.0
T001s_      80.1      0.0      0.0      180.0      4.052363      0.0      22.83809474      180.0
T002a_      80.0      0.0      0.0      180.0      3.86898842      0.0      21.75707365      180.0
T002b_      79.0      0.0      0.0      180.0      2.28325929      0.0      15.93528178      180.0
T002c_      78.0      0.0      0.0      180.0      1.27500259      0.0      12.7812619      180.0
T002d_      77.0      0.0      0.0      180.0      0.62133562      0.0      10.73152487      180.0
T002e_      76.0      0.0      0.0      180.0      0.180872      0.0      9.27263959      180.0
T002f_      75.0      0.0      0.0      180.0      0.12722449      180.0      8.16741767      180.0
T002g_      74.0      0.0      0.0      0.0      0.352449      180.0      7.28929422      180.0
T002h_      73.0      0.0      0.0      0.0      0.52231314      180.0      6.56615778      180.0
T002i_      72.0      0.0      0.0      0.0      0.65684865      180.0      5.95428033      180.0
T002j_      71.0      0.0      0.0      0.0      0.76597958      180.0      5.42595976      180.0
T003a_      70.0      0.0      0.0      0.0      0.85841443      180.0      4.96024107      180.0
T003b_      69.0      0.0      0.0      0.0      0.93766326      180.0      4.54745652      180.0
T003c_      68.0      0.0      0.0      0.0      1.00674429      180.0      4.17835162      180.0
T003d_      67.0      0.0      0.0      0.0      1.06752974      180.0      3.84448434      180.0
T003e_      66.0      0.0      0.0      0.0      1.1210895      180.0      3.54160227      180.0
T003f_      65.0      0.0      0.0      0.0      1.16885298      180.0      3.26438168      180.0
T003g_      64.0      0.0      0.0      0.0      1.21091409      180.0      3.0102481      180.0
T003h_      63.0      0.0      0.0      0.0      1.24818167      180.0      2.77561165      180.0
T003i_      62.0      0.0      0.0      0.0      1.28043806      180.0      2.55830202      180.0
T003j_      61.0      0.0      0.0      0.0      1.30880293      180.0      2.35621518      180.0
T004a_      60.0      0.0      0.0      0.0      1.33299581      180.0      2.16762657      180.0
T004b_      59.0      0.0      0.0      0.0      1.35383036      180.0      1.99108983      180.0
T004c_      58.0      0.0      0.0      0.0      1.37103814      180.0      1.82484072      180.0
T004d_      59.0      0.0      0.0      0.0      1.35383036      180.0      1.99108983      180.0
T004e_      56.0      0.0      0.0      0.0      1.39642541      180.0      1.52144984      180.0
T004f_      55.0      0.0      0.0      0.0      1.4049564      180.0      1.38254522      180.0
T004g_      54.0      0.0      0.0      0.0      1.4110082      180.0      1.25056678      180.0
T004h_      53.0      0.0      0.0      0.0      1.41436974      180.0      1.12603982      180.0
T004i_      52.0      0.0      0.0      0.0      1.41562211      180.0      1.00746587      180.0
T004j_      51.0      0.0      0.0      0.0      1.41437841      180.0      0.89488486      180.0
T005a_      50.0      0.0      0.0      0.0      1.41151194      180.0      0.78760513      180.0
T005b_      49.0      0.0      0.0      0.0      1.40630373      180.0      0.68539424      180.0
T005c_      48.0      0.0      0.0      0.0      1.39939904      180.0      0.58778412      180.0
T005d_      47.0      0.0      0.0      0.0      1.39050771      180.0      0.49399406      180.0
T005e_      46.0      0.0      0.0      0.0      1.38017502      180.0      0.40450219      180.0
T005f_      45.0      0.0      0.0      0.0      1.36812477      180.0      0.31823026      180.0
T005g_      44.0      0.0      0.0      0.0      1.35479342      180.0      0.23565258      180.0
T005h_      43.0      0.0      0.0      0.0      1.3400462      180.0      0.15586796      180.0
T005i_      42.0      0.0      0.0      0.0      1.32396356      180.0      0.07969738      180.0
T005j_      41.0      0.0      0.0      0.0      1.30671317      180.0      0.00610775      180.0
T006a_      40.0      0.0      0.0      0.0      1.28829066      180.0      0.06421434      0.0
T006b_      39.0      0.0      0.0      0.0      1.26893038      180.0      0.13200816      0.0
T006c_      38.0      0.0      0.0      0.0      1.24813552      180.0      0.19683471      0.0
T006d_      37.0      0.0      0.0      0.0      1.22655363      180.0      0.25924231      0.0
T006e_      36.0      0.0      0.0      0.0      1.20366189      180.0      0.31921142      0.0
T006f_      35.0      0.0      0.0      0.0      1.18013356      180.0      0.37662917      0.0
T006g_      34.0      0.0      0.0      0.0      1.15539099      180.0      0.43218835      0.0
T006h_      33.0      0.0      0.0      0.0      1.12992636      180.0      0.48534609      0.0
T006i_      32.0      0.0      0.0      0.0      1.10349553      180.0      0.53684596      0.0
T006j_      31.0      0.0      0.0      0.0      1.07626267      180.0      0.58607481      0.0
T007a_      30.0      0.0      0.0      0.0      1.04842903      180.0      0.63397259      0.0
T007b_      29.0      0.0      0.0      0.0      1.0198043      180.0      0.67966529      0.0
T007c_      28.0      0.0      0.0      0.0      0.99062509      180.0      0.72386338      0.0
T007d_      27.0      0.0      0.0      0.0      0.96064899      180.0      0.76594524      0.0
T007e_      26.0      0.0      0.0      0.0      0.93026179      180.0      0.80639058      0.0
T007f_      25.0      0.0      0.0      0.0      0.8991992      180.0      0.84482316      0.0
T007g_      24.0      0.0      0.0      0.0      0.86775677      180.0      0.88138556      0.0
T007h_      23.0      0.0      0.0      0.0      0.83543511      180.0      0.91631146      0.0
T007i_      22.0      0.0      0.0      0.0      0.8028562      180.0      0.94933882      0.0
T007j_      21.0      0.0      0.0      0.0      0.76953783      180.0      0.98079554      0.0
T008a_      20.0      0.0      0.0      0.0      0.7359275      180.0      1.01036083      0.0
T008b_      19.0      0.0      0.0      0.0      0.7017173      180.0      1.03896427      0.0
T008c_      18.0      0.0      0.0      0.0      0.66694195      180.0      1.06579259      0.0
T008d_      17.0      0.0      0.0      0.0      0.63186172      180.0      1.09163972      0.0
T008e_      16.0      0.0      0.0      0.0      0.5965173      180.0      1.11571431      0.0
T008f_      15.0      0.0      0.0      0.0      0.56103345      180.0      1.13836841      0.0
T008g_      14.0      0.0      0.0      0.0      0.52507047      180.0      1.15919372      0.0
T008h_      13.0      0.0      0.0      0.0      0.48897241      180.0      1.17860577      0.0
T008i_      12.0      0.0      0.0      0.0      0.45242794      180.0      1.19625957      0.0
T008j_      11.0      0.0      0.0      0.0      0.41580545      180.0      1.21216441      0.0
T009a_      10.0      0.0      0.0      0.0      0.37871276      180.0      1.2266496      0.0
T009b_      9.0      0.0      0.0      0.0      0.34164769      180.0      1.2395887      0.0
T009c_      8.0      0.0      0.0      0.0      0.30410055      180.0      1.25127609      0.0
T009d_      7.0      0.0      0.0      0.0      0.2662764      180.0      1.26143532      0.0
T009e_      6.0      0.0      0.0      0.0      0.22838051      180.0      1.2705311      0.0
T009f_      5.0      0.0      0.0      0.0      0.19037697      180.0      1.27831229      0.0
T009g_      4.0      0.0      0.0      0.0      0.15239922      180.0      1.28473957      0.0
T009h_      3.0      0.0      0.0      0.0      0.1141661      180.0      1.28997906      0.0
T009i_      2.0      0.0      0.0      0.0      0.07608204      180.0      1.29374914      0.0
T009j_      1.0      0.0      0.0      0.0      0.03788805      180.0      1.29618417      0.0
T010a_      0.0      0.0      0.0      180.0      0.0      0.0      1.29675793      0.0
T010b_      -1.0      0.0      0.0      180.0      0.03788805      0.0      1.29618417      0.0
T010c_      -2.0      0.0      0.0      180.0      0.07608204      0.0      1.29374914      0.0
T010d_      -3.0      0.0      0.0      180.0      0.1141661      0.0      1.28997906      0.0
T010e_      -4.0      0.0      0.0      180.0      0.15239922      0.0      1.28473957      0.0
T010f_      -5.0      0.0      0.0      180.0      0.19037697      0.0      1.27831229      0.0
T010g_      -6.0      0.0      0.0      180.0      0.22838051      0.0      1.2705311      0.0
T010h_      -7.0      0.0      0.0      180.0      0.2662764      0.0      1.26143532      0.0
T010i_      -8.0      0.0      0.0      180.0      0.30410055      0.0      1.25127609      0.0
T010j_      -9.0      0.0      0.0      180.0      0.34164769      0.0      1.2395887      0.0
T011a_      -10.0      0.0      0.0      180.0      0.37871276      0.0      1.2266496      0.0
T011b_      -11.0      0.0      0.0      180.0      0.41580545      0.0      1.21216441      0.0
T011c_      -12.0      0.0      0.0      180.0      0.45242794      0.0      1.19625957      0.0
T011d_      -13.0      0.0      0.0      180.0      0.48897241      0.0      1.17860577      0.0
T011e_      -14.0      0.0      0.0      180.0      0.52507047      0.0      1.15919372      0.0
T011f_      -15.0      0.0      0.0      180.0      0.56103345      0.0      1.13836841      0.0
T011g_      -16.0      0.0      0.0      180.0      0.5965173      0.0      1.11571431      0.0
T011h_      -17.0      0.0      0.0      180.0      0.63186172      0.0      1.09163972      0.0
T011i_      -18.0      0.0      0.0      180.0      0.66694195      0.0      1.06579259      0.0
T011j_      -19.0      0.0      0.0      180.0      0.7017173      0.0      1.03896427      0.0
T012a_      -20.0      0.0      0.0      180.0      0.7359275      0.0      1.01036083      0.0
T012b_      -21.0      0.0      0.0      180.0      0.76953783      0.0      0.98079554      0.0
T012c_      -22.0      0.0      0.0      180.0      0.8028562      0.0      0.94933882      0.0
T012d_      -23.0      0.0      0.0      180.0      0.83543511      0.0      0.91631146      0.0
T012e_      -24.0      0.0      0.0      180.0      0.86775677      0.0      0.88138556      0.0
T012f_      -25.0      0.0      0.0      180.0      0.8991992      0.0      0.84482316      0.0
T012g_      -26.0      0.0      0.0      180.0      0.93026179      0.0      0.80639058      0.0
T012h_      -27.0      0.0      0.0      180.0      0.96064899      0.0      0.76594524      0.0
T012i_      -28.0      0.0      0.0      180.0      0.99062509      0.0      0.72386338      0.0
T012j_      -29.0      0.0      0.0      180.0      1.0198043      0.0      0.67966529      0.0
T013a_      -30.0      0.0      0.0      180.0      1.04842903      0.0      0.63397259      0.0
T013b_      -31.0      0.0      0.0      180.0      1.07626267      0.0      0.58607481      0.0
T013c_      -32.0      0.0      0.0      180.0      1.10349553      0.0      0.53684596      0.0
T013d_      -33.0      0.0      0.0      180.0      1.12992636      0.0      0.48534609      0.0
T013e_      -34.0      0.0      0.0      180.0      1.15539099      0.0      0.43218835      0.0
T013f_      -35.0      0.0      0.0      180.0      1.18013356      0.0      0.37662917      0.0
T013g_      -36.0      0.0      0.0      180.0      1.20366189      0.0      0.31921142      0.0
T013h_      -37.0      0.0      0.0      180.0      1.22655363      0.0      0.25924231      0.0
T013i_      -38.0      0.0      0.0      180.0      1.24813552      0.0      0.19683471      0.0
T013j_      -39.0      0.0      0.0      180.0      1.26893038      0.0      0.13200816      0.0
T014a_      -40.0      0.0      0.0      180.0      1.28829066      0.0      0.06421434      0.0
T014b_      -41.0      0.0      0.0      180.0      1.30671317      0.0      0.00610775      180.0
T014c_      -42.0      0.0      0.0      180.0      1.32396356      0.0      0.07969738      180.0
T014d_      -43.0      0.0      0.0      180.0      1.3400462      0.0      0.15586796      180.0
T014e_      -44.0      0.0      0.0      180.0      1.35479342      0.0      0.23565258      180.0
T014f_      -45.0      0.0      0.0      180.0      1.36812477      0.0      0.31823026      180.0
T014g_      -46.0      0.0      0.0      180.0      1.38017502      0.0      0.40450219      180.0
T014h_      -47.0      0.0      0.0      180.0      1.39050771      0.0      0.49399406      180.0
T014i_      -48.0      0.0      0.0      180.0      1.39939904      0.0      0.58778412      180.0
T014j_      -49.0      0.0      0.0      180.0      1.40630373      0.0      0.68539424      180.0
T015a_      -50.0      0.0      0.0      180.0      1.41151194      0.0      0.78760513      180.0
T015b_      -51.0      0.0      0.0      180.0      1.41437841      0.0      0.89488486      180.0
T015c_      -52.0      0.0      0.0      180.0      1.41562211      0.0      1.00746587      180.0
T015d_      -53.0      0.0      0.0      180.0      1.41436974      0.0      1.12603982      180.0
T015e_      -54.0      0.0      0.0      180.0      1.4110082      0.0      1.25056678      180.0
T015f_      -55.0      0.0      0.0      180.0      1.4049564      0.0      1.38254522      180.0
T015g_      -56.0      0.0      0.0      180.0      1.39642541      0.0      1.52144984      180.0
T015h_      -57.0      0.0      0.0      180.0      1.38530625      0.0      1.66897663      180.0
T015i_      -58.0      0.0      0.0      180.0      1.37103814      0.0      1.82484072      180.0
T015j_      -59.0      0.0      0.0      180.0      1.35383036      0.0      1.99108983      180.0
T016a_      -60.0      0.0      0.0      180.0      1.33299581      0.0      2.16762657      180.0
T016b_      -61.0      0.0      0.0      180.0      1.30880293      0.0      2.35621518      180.0
T016c_      -62.0      0.0      0.0      180.0      1.28043806      0.0      2.55830202      180.0
T016d_      -63.0      0.0      0.0      180.0      1.24818167      0.0      2.77561165      180.0
T016e_      -64.0      0.0      0.0      180.0      1.21091409      0.0      3.0102481      180.0
T016f_      -65.0      0.0      0.0      180.0      1.16885298      0.0      3.26438168      180.0
T016g_      -66.0      0.0      0.0      180.0      1.1210895      0.0      3.54160227      180.0
T016h_      -67.0      0.0      0.0      180.0      1.06752974      0.0      3.84448434      180.0
T016i_      -68.0      0.0      0.0      180.0      1.00674429      0.0      4.17835162      180.0
T016j_      -69.0      0.0      0.0      180.0      0.93766326      0.0      4.54745652      180.0
T017a_      -70.0      0.0      0.0      180.0      0.85841443      0.0      4.96024107      180.0
T017b_      -71.0      0.0      0.0      180.0      0.76597958      0.0      5.42595976      180.0
T017c_      -72.0      0.0      0.0      180.0      0.65684865      0.0      5.95428033      180.0
T017d_      -73.0      0.0      0.0      180.0      0.52231314      0.0      6.56615778      180.0
T017e_      -74.0      0.0      0.0      180.0      0.352449      0.0      7.28929422      180.0
T017f_      -75.0      0.0      0.0      180.0      0.12722449      0.0      8.16741767      180.0
T017g_      -76.0      0.0      0.0      180.0      0.180872      180.0      9.27263959      180.0
T017h_      -77.0      0.0      0.0      0.0      0.62133562      180.0      10.73152487      180.0
T017i_      -78.0      0.0      0.0      0.0      1.27500259      180.0      12.7812619      180.0
T017j_      -79.0      0.0      0.0      180.0      2.28325929      180.0      15.93528178      180.0
T018a_      -80.0      0.0      0.0      180.0      3.86898842      180.0      21.75707365      180.0
T018k_      -80.1      0.0      0.0      180.0      4.052363      180.0      22.83809474      180.0
T018l_      -80.2      0.0      0.0      180.0      4.19552968      180.0      24.03711435      180.0
T018m_      -80.3      0.0      0.0      180.0      4.30424161      180.0      25.29376817      180.0
T018n_      -80.4      0.0      0.0      180.0      4.37982567      180.0      26.58517073      180.0
T018o_      -80.5      0.0      0.0      180.0      4.42189353      180.0      27.89517962      180.0
T018p_      -80.6      0.0      0.0      180.0      4.42997827      180.0      29.20882048      180.0
T018q_      -80.7      0.0      0.0      180.0      4.40390437      180.0      30.51089519      180.0
T018r_      -80.8      0.0      0.0      180.0      4.34341227      180.0      31.78490866      180.0
T018s_      -80.9      0.0      0.0      180.0      4.24662549      180.0      33.00763662      180.0
T018b_      -81.0      0.0      0.0      180.0      4.10714024      180.0      34.11818436      180.0
T018c_      -82.0      0.0      0.0      180.0      2.81548527      180.0      40.36640718      180.0
T018d_      -83.0      0.0      0.0      180.0      1.93960567      180.0      43.96385835      180.0
T018e_      -84.0      0.0      0.0      180.0      1.34635839      180.0      46.36531857      180.0
T018f_      -85.0      0.0      0.0      180.0      0.93439901      180.0      48.05580338      180.0
T018g_      -86.0      0.0      0.0      180.0      0.6419881      180.0      49.26569814      180.0
T018h_      -87.0      0.0      0.0      180.0      0.42542167      180.0      50.12088345      180.0
T018i_      -88.0      0.0      0.0      180.0      0.25962724      180.0      50.69170405      180.0
T018j_      -89.0      0.0      0.0      180.0      0.12109765      180.0      51.01319722      180.0
T019a_      -89.99      0.0      0.0      0.0      0.0011751      180.0      51.13565413      180.0
