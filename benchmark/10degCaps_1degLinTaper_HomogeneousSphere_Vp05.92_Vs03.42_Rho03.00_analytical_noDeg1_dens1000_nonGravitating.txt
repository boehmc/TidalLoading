Station  Lat(+N,deg)  Lon(+E,deg)  E-Amp(mm)  E-Pha(deg)  N-Amp(mm)  N-Pha(deg)  V-Amp(mm)  V-Pha(deg) 
T001a_      89.99      0.0      0.0      0.0      0.00916676      0.0      115.24401666      180.0
T001b_      89.0      0.0      0.0      0.0      0.91321242      0.0      114.85341102      180.0
T001c_      88.0      0.0      0.0      0.0      1.84557683      0.0      113.8303098      180.0
T001d_      87.0      0.0      0.0      0.0      2.79664214      0.0      112.06274705      180.0
T001e_      86.0      0.0      0.0      0.0      3.78048893      0.0      109.51527581      180.0
T001f_      85.0      0.0      0.0      180.0      4.809761      0.0      106.10732609      180.0
T001g_      84.0      0.0      0.0      180.0      5.89854301      0.0      101.70535304      180.0
T001h_      83.0      0.0      0.0      180.0      7.06357547      0.0      96.07833943      180.0
T001i_      82.0      0.0      0.0      180.0      8.32664315      0.0      88.76613854      180.0
T001j_      81.0      0.0      0.0      180.0      9.72164554      0.0      78.3592465      180.0
T001k_      80.9      0.0      0.0      180.0      9.84895552      0.0      76.90010153      180.0
T001l_      80.8      0.0      0.0      180.0      9.9375437      0.0      75.34771616      180.0
T001m_      80.7      0.0      0.0      180.0      9.98715471      0.0      73.74147656      180.0
T001n_      80.6      0.0      0.0      180.0      9.99811749      0.0      72.10381989      180.0
T001o_      80.5      0.0      0.0      180.0      9.97096568      0.0      70.4527642      180.0
T001p_      80.4      0.0      0.0      180.0      9.90603089      0.0      68.80626754      180.0
T001q_      80.3      0.0      0.0      180.0      9.80368232      0.0      67.18169183      180.0
T001r_      80.2      0.0      0.0      180.0      9.66418583      0.0      65.59640352      180.0
T001s_      80.1      0.0      0.0      180.0      9.48768098      0.0      64.07239457      180.0
T002a_      80.0      0.0      0.0      180.0      9.27520161      0.0      62.64840893      180.0
T002b_      79.0      0.0      0.0      180.0      7.20528064      0.0      52.76926661      180.0
T002c_      78.0      0.0      0.0      180.0      5.52093991      0.0      46.02560527      180.0
T002d_      77.0      0.0      0.0      0.0      4.12033848      0.0      40.8240337      180.0
T002e_      76.0      0.0      0.0      180.0      2.93768624      0.0      36.60948694      180.0
T002f_      75.0      0.0      0.0      180.0      1.92714021      0.0      33.09072082      180.0
T002g_      74.0      0.0      0.0      180.0      1.05540682      0.0      30.09046352      180.0
T002h_      73.0      0.0      0.0      180.0      0.29760659      0.0      27.49127852      180.0
T002i_      72.0      0.0      0.0      0.0      0.36527559      180.0      25.21113158      180.0
T002j_      71.0      0.0      0.0      0.0      0.94759764      180.0      23.19247006      180.0
T003a_      70.0      0.0      0.0      0.0      1.46384074      180.0      21.37596903      180.0
T003b_      69.0      0.0      0.0      0.0      1.9203032      180.0      19.74476922      180.0
T003c_      68.0      0.0      0.0      0.0      2.32579896      180.0      18.26479576      180.0
T003d_      67.0      0.0      0.0      0.0      2.68666605      180.0      16.91423076      180.0
T003e_      66.0      0.0      0.0      0.0      3.00819214      180.0      15.67548854      180.0
T003f_      65.0      0.0      0.0      0.0      3.29477379      180.0      14.53403249      180.0
T003g_      64.0      0.0      0.0      0.0      3.55014258      180.0      13.47790543      180.0
T003h_      63.0      0.0      0.0      0.0      3.77747451      180.0      12.49706741      180.0
T003i_      62.0      0.0      0.0      0.0      3.97949105      180.0      11.58309028      180.0
T003j_      61.0      0.0      0.0      0.0      4.15856338      180.0      10.72878309      180.0
T004a_      60.0      0.0      0.0      0.0      4.316711      180.0      9.92803456      180.0
T004b_      59.0      0.0      0.0      0.0      4.45570619      180.0      9.17552462      180.0
T004c_      58.0      0.0      0.0      0.0      4.57714798      180.0      8.46673848      180.0
T004d_      59.0      0.0      0.0      0.0      4.45570619      180.0      9.17552462      180.0
T004e_      56.0      0.0      0.0      0.0      4.77259361      180.0      7.16478841      180.0
T004f_      55.0      0.0      0.0      0.0      4.84894676      180.0      6.56514501      180.0
T004g_      54.0      0.0      0.0      0.0      4.91234367      180.0      5.99597304      180.0
T004h_      53.0      0.0      0.0      0.0      4.96368593      180.0      5.4548931      180.0
T004i_      52.0      0.0      0.0      0.0      5.00377635      180.0      4.93994521      180.0
T004j_      51.0      0.0      0.0      0.0      5.03325656      180.0      4.44898709      180.0
T005a_      50.0      0.0      0.0      0.0      5.05286884      180.0      3.9805846      180.0
T005b_      49.0      0.0      0.0      0.0      5.06310205      180.0      3.53315771      180.0
T005c_      48.0      0.0      0.0      0.0      5.06454483      180.0      3.1053594      180.0
T005d_      47.0      0.0      0.0      0.0      5.05763469      180.0      2.69593117      180.0
T005e_      46.0      0.0      0.0      0.0      5.0429411      180.0      2.30381407      180.0
T005f_      45.0      0.0      0.0      0.0      5.0207108      180.0      1.92802494      180.0
T005g_      44.0      0.0      0.0      0.0      4.99146393      180.0      1.56775211      180.0
T005h_      43.0      0.0      0.0      0.0      4.95547574      180.0      1.22193947      180.0
T005i_      42.0      0.0      0.0      0.0      4.91314673      180.0      0.89007027      180.0
T005j_      41.0      0.0      0.0      0.0      4.86473889      180.0      0.57145179      180.0
T006a_      40.0      0.0      0.0      0.0      4.81053764      180.0      0.26534199      180.0
T006b_      39.0      0.0      0.0      0.0      4.75083994      180.0      0.02871458      0.0
T006c_      38.0      0.0      0.0      0.0      4.6859086      180.0      0.31119356      0.0
T006d_      37.0      0.0      0.0      0.0      4.61594217      180.0      0.58266228      0.0
T006e_      36.0      0.0      0.0      0.0      4.5411812      180.0      0.84352716      0.0
T006f_      35.0      0.0      0.0      0.0      4.46185331      180.0      1.09417639      0.0
T006g_      34.0      0.0      0.0      0.0      4.378186      180.0      1.33493253      0.0
T006h_      33.0      0.0      0.0      0.0      4.29031648      180.0      1.56619251      0.0
T006i_      32.0      0.0      0.0      0.0      4.19849602      180.0      1.78825066      0.0
T006j_      31.0      0.0      0.0      0.0      4.1028672      180.0      2.00135776      0.0
T007a_      30.0      0.0      0.0      0.0      4.00357805      180.0      2.20576132      0.0
T007b_      29.0      0.0      0.0      0.0      3.90082546      180.0      2.40175143      0.0
T007c_      28.0      0.0      0.0      0.0      3.79477251      180.0      2.58954539      0.0
T007d_      27.0      0.0      0.0      0.0      3.68555466      180.0      2.76931771      0.0
T007e_      26.0      0.0      0.0      0.0      3.5732819      180.0      2.94122595      0.0
T007f_      25.0      0.0      0.0      0.0      3.45817589      180.0      3.10555254      0.0
T007g_      24.0      0.0      0.0      0.0      3.34032012      180.0      3.2623455      0.0
T007h_      23.0      0.0      0.0      0.0      3.2198433      180.0      3.41183056      0.0
T007i_      22.0      0.0      0.0      0.0      3.09692138      180.0      3.55410527      0.0
T007j_      21.0      0.0      0.0      0.0      2.97163962      180.0      3.68931213      0.0
T008a_      20.0      0.0      0.0      0.0      2.84413302      180.0      3.81753528      0.0
T008b_      19.0      0.0      0.0      0.0      2.71453278      180.0      3.93895635      0.0
T008c_      18.0      0.0      0.0      0.0      2.58295614      180.0      4.05360252      0.0
T008d_      17.0      0.0      0.0      0.0      2.44950511      180.0      4.1616163      0.0
T008e_      16.0      0.0      0.0      0.0      2.31432847      180.0      4.26304725      0.0
T008f_      15.0      0.0      0.0      0.0      2.17748259      180.0      4.35798255      0.0
T008g_      14.0      0.0      0.0      0.0      2.03913661      180.0      4.44648309      0.0
T008h_      13.0      0.0      0.0      0.0      1.89935079      180.0      4.52860983      0.0
T008i_      12.0      0.0      0.0      0.0      1.75823991      180.0      4.60445574      0.0
T008j_      11.0      0.0      0.0      0.0      1.61595182      180.0      4.67409413      0.0
T009a_      10.0      0.0      0.0      0.0      1.47254005      180.0      4.73748445      0.0
T009b_      9.0      0.0      0.0      0.0      1.32869047      180.0      4.79444432      0.0
T009c_      8.0      0.0      0.0      0.0      1.1831048      180.0      4.84545068      0.0
T009d_      7.0      0.0      0.0      0.0      1.03689964      180.0      4.89038311      0.0
T009e_      6.0      0.0      0.0      0.0      0.89005384      180.0      4.92929861      0.0
T009f_      5.0      0.0      0.0      0.0      0.74260581      180.0      4.96217801      0.0
T009g_      4.0      0.0      0.0      0.0      0.59467422      180.0      4.98906856      0.0
T009h_      3.0      0.0      0.0      0.0      0.44636106      180.0      5.00999153      0.0
T009i_      2.0      0.0      0.0      0.0      0.29774343      180.0      5.0249001      0.0
T009j_      1.0      0.0      0.0      0.0      0.14892324      180.0      5.03384472      0.0
T010a_      0.0      0.0      0.0      180.0      0.0      0.0      5.03682608      0.0
T010b_      -1.0      0.0      0.0      180.0      0.14892324      0.0      5.03384472      0.0
T010c_      -2.0      0.0      0.0      180.0      0.29774343      0.0      5.0249001      0.0
T010d_      -3.0      0.0      0.0      180.0      0.44636106      0.0      5.00999153      0.0
T010e_      -4.0      0.0      0.0      180.0      0.59467422      0.0      4.98906856      0.0
T010f_      -5.0      0.0      0.0      180.0      0.74260581      0.0      4.96217801      0.0
T010g_      -6.0      0.0      0.0      180.0      0.89005384      0.0      4.92929861      0.0
T010h_      -7.0      0.0      0.0      180.0      1.03689964      0.0      4.89038311      0.0
T010i_      -8.0      0.0      0.0      180.0      1.1831048      0.0      4.84545068      0.0
T010j_      -9.0      0.0      0.0      180.0      1.32869047      0.0      4.79444432      0.0
T011a_      -10.0      0.0      0.0      180.0      1.47254005      0.0      4.73748445      0.0
T011b_      -11.0      0.0      0.0      180.0      1.61595182      0.0      4.67409413      0.0
T011c_      -12.0      0.0      0.0      180.0      1.75823991      0.0      4.60445574      0.0
T011d_      -13.0      0.0      0.0      180.0      1.89935079      0.0      4.52860983      0.0
T011e_      -14.0      0.0      0.0      180.0      2.03913661      0.0      4.44648309      0.0
T011f_      -15.0      0.0      0.0      180.0      2.17748259      0.0      4.35798255      0.0
T011g_      -16.0      0.0      0.0      180.0      2.31432847      0.0      4.26304725      0.0
T011h_      -17.0      0.0      0.0      180.0      2.44950511      0.0      4.1616163      0.0
T011i_      -18.0      0.0      0.0      180.0      2.58295614      0.0      4.05360252      0.0
T011j_      -19.0      0.0      0.0      180.0      2.71453278      0.0      3.93895635      0.0
T012a_      -20.0      0.0      0.0      180.0      2.84413302      0.0      3.81753528      0.0
T012b_      -21.0      0.0      0.0      180.0      2.97163962      0.0      3.68931213      0.0
T012c_      -22.0      0.0      0.0      180.0      3.09692138      0.0      3.55410527      0.0
T012d_      -23.0      0.0      0.0      180.0      3.2198433      0.0      3.41183056      0.0
T012e_      -24.0      0.0      0.0      180.0      3.34032012      0.0      3.2623455      0.0
T012f_      -25.0      0.0      0.0      180.0      3.45817589      0.0      3.10555254      0.0
T012g_      -26.0      0.0      0.0      180.0      3.5732819      0.0      2.94122595      0.0
T012h_      -27.0      0.0      0.0      180.0      3.68555466      0.0      2.76931771      0.0
T012i_      -28.0      0.0      0.0      180.0      3.79477251      0.0      2.58954539      0.0
T012j_      -29.0      0.0      0.0      180.0      3.90082546      0.0      2.40175143      0.0
T013a_      -30.0      0.0      0.0      180.0      4.00357805      0.0      2.20576132      0.0
T013b_      -31.0      0.0      0.0      180.0      4.1028672      0.0      2.00135776      0.0
T013c_      -32.0      0.0      0.0      180.0      4.19849602      0.0      1.78825066      0.0
T013d_      -33.0      0.0      0.0      180.0      4.29031648      0.0      1.56619251      0.0
T013e_      -34.0      0.0      0.0      180.0      4.378186      0.0      1.33493253      0.0
T013f_      -35.0      0.0      0.0      180.0      4.46185331      0.0      1.09417639      0.0
T013g_      -36.0      0.0      0.0      180.0      4.5411812      0.0      0.84352716      0.0
T013h_      -37.0      0.0      0.0      180.0      4.61594217      0.0      0.58266228      0.0
T013i_      -38.0      0.0      0.0      180.0      4.6859086      0.0      0.31119356      0.0
T013j_      -39.0      0.0      0.0      180.0      4.75083994      0.0      0.02871458      0.0
T014a_      -40.0      0.0      0.0      180.0      4.81053764      0.0      0.26534199      180.0
T014b_      -41.0      0.0      0.0      180.0      4.86473889      0.0      0.57145179      180.0
T014c_      -42.0      0.0      0.0      180.0      4.91314673      0.0      0.89007027      180.0
T014d_      -43.0      0.0      0.0      180.0      4.95547574      0.0      1.22193947      180.0
T014e_      -44.0      0.0      0.0      180.0      4.99146393      0.0      1.56775211      180.0
T014f_      -45.0      0.0      0.0      180.0      5.0207108      0.0      1.92802494      180.0
T014g_      -46.0      0.0      0.0      180.0      5.0429411      0.0      2.30381407      180.0
T014h_      -47.0      0.0      0.0      180.0      5.05763469      0.0      2.69593117      180.0
T014i_      -48.0      0.0      0.0      180.0      5.06454483      0.0      3.1053594      180.0
T014j_      -49.0      0.0      0.0      180.0      5.06310205      0.0      3.53315771      180.0
T015a_      -50.0      0.0      0.0      180.0      5.05286884      0.0      3.9805846      180.0
T015b_      -51.0      0.0      0.0      180.0      5.03325656      0.0      4.44898709      180.0
T015c_      -52.0      0.0      0.0      180.0      5.00377635      0.0      4.93994521      180.0
T015d_      -53.0      0.0      0.0      180.0      4.96368593      0.0      5.4548931      180.0
T015e_      -54.0      0.0      0.0      180.0      4.91234367      0.0      5.99597304      180.0
T015f_      -55.0      0.0      0.0      180.0      4.84894676      0.0      6.56514501      180.0
T015g_      -56.0      0.0      0.0      180.0      4.77259361      0.0      7.16478841      180.0
T015h_      -57.0      0.0      0.0      180.0      4.68236163      0.0      7.79766619      180.0
T015i_      -58.0      0.0      0.0      180.0      4.57714798      0.0      8.46673848      180.0
T015j_      -59.0      0.0      0.0      180.0      4.45570619      0.0      9.17552462      180.0
T016a_      -60.0      0.0      0.0      180.0      4.316711      0.0      9.92803456      180.0
T016b_      -61.0      0.0      0.0      180.0      4.15856338      0.0      10.72878309      180.0
T016c_      -62.0      0.0      0.0      180.0      3.97949105      0.0      11.58309028      180.0
T016d_      -63.0      0.0      0.0      180.0      3.77747451      0.0      12.49706741      180.0
T016e_      -64.0      0.0      0.0      180.0      3.55014258      0.0      13.47790543      180.0
T016f_      -65.0      0.0      0.0      180.0      3.29477379      0.0      14.53403249      180.0
T016g_      -66.0      0.0      0.0      180.0      3.00819214      0.0      15.67548854      180.0
T016h_      -67.0      0.0      0.0      180.0      2.68666605      0.0      16.91423076      180.0
T016i_      -68.0      0.0      0.0      180.0      2.32579896      0.0      18.26479576      180.0
T016j_      -69.0      0.0      0.0      180.0      1.9203032      0.0      19.74476922      180.0
T017a_      -70.0      0.0      0.0      180.0      1.46384074      0.0      21.37596903      180.0
T017b_      -71.0      0.0      0.0      180.0      0.94759764      0.0      23.19247006      180.0
T017c_      -72.0      0.0      0.0      180.0      0.36527559      0.0      25.21113158      180.0
T017d_      -73.0      0.0      0.0      0.0      0.29760659      180.0      27.49127852      180.0
T017e_      -74.0      0.0      0.0      180.0      1.05540682      180.0      30.09046352      180.0
T017f_      -75.0      0.0      0.0      0.0      1.92714021      180.0      33.09072082      180.0
T017g_      -76.0      0.0      0.0      0.0      2.93768624      180.0      36.60948694      180.0
T017h_      -77.0      0.0      0.0      0.0      4.12033848      180.0      40.8240337      180.0
T017i_      -78.0      0.0      0.0      0.0      5.52093991      180.0      46.02560527      180.0
T017j_      -79.0      0.0      0.0      0.0      7.20528064      180.0      52.76926661      180.0
T018a_      -80.0      0.0      0.0      180.0      9.27520161      180.0      62.64840893      180.0
T018k_      -80.1      0.0      0.0      180.0      9.48768098      180.0      64.07239457      180.0
T018l_      -80.2      0.0      0.0      180.0      9.66418583      180.0      65.59640352      180.0
T018m_      -80.3      0.0      0.0      180.0      9.80368232      180.0      67.18169183      180.0
T018n_      -80.4      0.0      0.0      180.0      9.90603089      180.0      68.80626754      180.0
T018o_      -80.5      0.0      0.0      180.0      9.97096568      180.0      70.4527642      180.0
T018p_      -80.6      0.0      0.0      180.0      9.99811749      180.0      72.10381989      180.0
T018q_      -80.7      0.0      0.0      180.0      9.98715471      180.0      73.74147656      180.0
T018r_      -80.8      0.0      0.0      180.0      9.9375437      180.0      75.34771616      180.0
T018s_      -80.9      0.0      0.0      180.0      9.84895552      180.0      76.90010153      180.0
T018b_      -81.0      0.0      0.0      180.0      9.72164554      180.0      78.3592465      180.0
T018c_      -82.0      0.0      0.0      180.0      8.32664315      180.0      88.76613854      180.0
T018d_      -83.0      0.0      0.0      180.0      7.06357547      180.0      96.07833943      180.0
T018e_      -84.0      0.0      0.0      180.0      5.89854301      180.0      101.70535304      180.0
T018f_      -85.0      0.0      0.0      180.0      4.809761      180.0      106.10732609      180.0
T018g_      -86.0      0.0      0.0      180.0      3.78048893      180.0      109.51527581      180.0
T018h_      -87.0      0.0      0.0      0.0      2.79664214      180.0      112.06274705      180.0
T018i_      -88.0      0.0      0.0      180.0      1.84557683      180.0      113.8303098      180.0
T018j_      -89.0      0.0      0.0      180.0      0.91321242      180.0      114.85341102      180.0
T019a_      -89.99      0.0      0.0      180.0      0.00916676      180.0      115.24401666      180.0
