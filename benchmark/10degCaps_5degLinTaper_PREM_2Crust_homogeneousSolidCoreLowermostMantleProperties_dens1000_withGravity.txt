Station  Lat(+N,deg)  Lon(+E,deg)  E-Amp(mm)  E-Pha(deg)  N-Amp(mm)  N-Pha(deg)  V-Amp(mm)  V-Pha(deg) 
T001a_      89.99      0.0      0.0      0.0      0.00485373      0.0      33.61783197      180.0
T001b_      89.0      0.0      0.0      180.0      0.48889566      0.0      33.48247301      180.0
T001c_      88.0      0.0      0.0      180.0      1.0024368      0.0      33.06261597      180.0
T001d_      87.0      0.0      0.0      180.0      1.56920606      0.0      32.29336617      180.0
T001e_      86.0      0.0      0.0      180.0      2.22852769      0.0      31.03120073      180.0
T001f_      85.0      0.0      0.0      180.0      3.03418257      0.0      28.89743912      180.0
T001g_      84.0      0.0      0.0      180.0      3.70488552      0.0      24.95336643      180.0
T001h_      83.0      0.0      0.0      180.0      4.00243053      0.0      20.36290613      180.0
T001i_      82.0      0.0      0.0      180.0      3.99649211      0.0      15.65216998      180.0
T001j_      81.0      0.0      0.0      180.0      3.69752498      0.0      11.17286116      180.0
T001k_      80.9      0.0      0.0      180.0      3.65044429      0.0      10.75356225      180.0
T001l_      80.8      0.0      0.0      180.0      3.60006884      0.0      10.3416563      180.0
T001m_      80.7      0.0      0.0      180.0      3.54633349      0.0      9.93790638      180.0
T001n_      80.6      0.0      0.0      180.0      3.48919603      0.0      9.54313463      180.0
T001o_      80.5      0.0      0.0      180.0      3.4286163      0.0      9.15831286      180.0
T001p_      80.4      0.0      0.0      180.0      3.36456842      0.0      8.78460748      180.0
T001q_      80.3      0.0      0.0      180.0      3.29706045      0.0      8.4234121      180.0
T001r_      80.2      0.0      0.0      180.0      3.22608252      0.0      8.07652316      180.0
T001s_      80.1      0.0      0.0      180.0      3.1513316      0.0      7.74698789      180.0
T002a_      80.0      0.0      0.0      180.0      3.07173383      0.0      7.44425099      180.0
T002b_      79.0      0.0      0.0      180.0      2.40986623      0.0      5.54029398      180.0
T002c_      78.0      0.0      0.0      180.0      1.96605092      0.0      4.41154788      180.0
T002d_      77.0      0.0      0.0      180.0      1.65697048      0.0      3.64878038      180.0
T002e_      76.0      0.0      0.0      180.0      1.43193202      0.0      3.09502669      180.0
T002f_      75.0      0.0      0.0      180.0      1.26092326      0.0      2.67188056      180.0
T002g_      74.0      0.0      0.0      180.0      1.12552647      0.0      2.33556681      180.0
T002h_      73.0      0.0      0.0      180.0      1.0144758      0.0      2.060454      180.0
T002i_      72.0      0.0      0.0      180.0      0.9206195      0.0      1.83040081      180.0
T002j_      71.0      0.0      0.0      180.0      0.83937807      0.0      1.63479234      180.0
T003a_      70.0      0.0      0.0      180.0      0.76772526      0.0      1.46624419      180.0
T003b_      69.0      0.0      0.0      180.0      0.70385818      0.0      1.31990208      180.0
T003c_      68.0      0.0      0.0      180.0      0.64631572      0.0      1.19168836      180.0
T003d_      67.0      0.0      0.0      180.0      0.5941336      0.0      1.07863734      180.0
T003e_      66.0      0.0      0.0      180.0      0.5465997      0.0      0.97841632      180.0
T003f_      65.0      0.0      0.0      180.0      0.50316947      0.0      0.8891555      180.0
T003g_      64.0      0.0      0.0      180.0      0.46340933      0.0      0.80933103      180.0
T003h_      63.0      0.0      0.0      180.0      0.4269572      0.0      0.7376837      180.0
T003i_      62.0      0.0      0.0      180.0      0.39350083      0.0      0.67315745      180.0
T003j_      61.0      0.0      0.0      180.0      0.36276517      0.0      0.61486002      180.0
T004a_      60.0      0.0      0.0      180.0      0.33450596      0.0      0.56203432      180.0
T004b_      59.0      0.0      0.0      180.0      0.3085056      0.0      0.51403475      180.0
T004c_      58.0      0.0      0.0      180.0      0.28456754      0.0      0.47030641      180.0
T004d_      59.0      0.0      0.0      180.0      0.3085056      0.0      0.51403475      180.0
T004e_      56.0      0.0      0.0      180.0      0.24218839      0.0      0.39381455      180.0
T004f_      55.0      0.0      0.0      180.0      0.22344227      0.0      0.36027876      180.0
T004g_      54.0      0.0      0.0      0.0      0.20614324      0.0      0.32944969      180.0
T004h_      53.0      0.0      0.0      180.0      0.19017143      0.0      0.30105285      180.0
T004i_      52.0      0.0      0.0      180.0      0.17541818      0.0      0.27484789      180.0
T004j_      51.0      0.0      0.0      180.0      0.16178341      0.0      0.25062241      180.0
T005a_      50.0      0.0      0.0      180.0      0.14917625      0.0      0.22819011      180.0
T005b_      49.0      0.0      0.0      180.0      0.13751409      0.0      0.20738382      180.0
T005c_      48.0      0.0      0.0      0.0      0.12672212      0.0      0.18805644      180.0
T005d_      47.0      0.0      0.0      180.0      0.11673299      0.0      0.17007682      180.0
T005e_      46.0      0.0      0.0      0.0      0.10748388      0.0      0.15332711      180.0
T005f_      45.0      0.0      0.0      180.0      0.09891785      0.0      0.13770349      180.0
T005g_      44.0      0.0      0.0      180.0      0.09098305      0.0      0.12311339      180.0
T005h_      43.0      0.0      0.0      0.0      0.08363287      0.0      0.1094723      180.0
T005i_      42.0      0.0      0.0      0.0      0.07682211      0.0      0.09670733      180.0
T005j_      41.0      0.0      0.0      180.0      0.07051011      0.0      0.08475147      180.0
T006a_      40.0      0.0      0.0      0.0      0.06466146      0.0      0.07354326      180.0
T006b_      39.0      0.0      0.0      180.0      0.05924159      0.0      0.06302889      180.0
T006c_      38.0      0.0      0.0      180.0      0.05422023      0.0      0.05315785      180.0
T006d_      37.0      0.0      0.0      180.0      0.04956792      0.0      0.04388521      180.0
T006e_      36.0      0.0      0.0      0.0      0.04526055      0.0      0.03516905      180.0
T006f_      35.0      0.0      0.0      180.0      0.04127244      0.0      0.02697235      180.0
T006g_      34.0      0.0      0.0      180.0      0.03758225      0.0      0.01926098      180.0
T006h_      33.0      0.0      0.0      180.0      0.03416997      0.0      0.01200308      180.0
T006i_      32.0      0.0      0.0      0.0      0.03101605      0.0      0.00517098      180.0
T006j_      31.0      0.0      0.0      0.0      0.02810402      0.0      0.00126201      0.0
T007a_      30.0      0.0      0.0      0.0      0.0254177      0.0      0.0073196      0.0
T007b_      29.0      0.0      0.0      180.0      0.02294077      0.0      0.01302322      0.0
T007c_      28.0      0.0      0.0      180.0      0.02066099      0.0      0.01839363      0.0
T007d_      27.0      0.0      0.0      180.0      0.01856487      0.0      0.02344903      0.0
T007e_      26.0      0.0      0.0      180.0      0.01663991      0.0      0.02820653      0.0
T007f_      25.0      0.0      0.0      180.0      0.01487531      0.0      0.03268167      0.0
T007g_      24.0      0.0      0.0      180.0      0.01326134      0.0      0.03688951      0.0
T007h_      23.0      0.0      0.0      180.0      0.01178673      0.0      0.04084254      0.0
T007i_      22.0      0.0      0.0      0.0      0.01044101      0.0      0.0445527      0.0
T007j_      21.0      0.0      0.0      0.0      0.00921903      0.0      0.04803214      0.0
T008a_      20.0      0.0      0.0      180.0      0.00811058      0.0      0.05129079      0.0
T008b_      19.0      0.0      0.0      180.0      0.00710717      0.0      0.05433795      0.0
T008c_      18.0      0.0      0.0      180.0      0.00620119      0.0      0.05718236      0.0
T008d_      17.0      0.0      0.0      180.0      0.00538913      0.0      0.05983281      0.0
T008e_      16.0      0.0      0.0      180.0      0.00466033      0.0      0.06229588      0.0
T008f_      15.0      0.0      0.0      180.0      0.00400978      0.0      0.06457863      0.0
T008g_      14.0      0.0      0.0      180.0      0.00343139      0.0      0.06668724      0.0
T008h_      13.0      0.0      0.0      180.0      0.00291986      0.0      0.06862758      0.0
T008i_      12.0      0.0      0.0      0.0      0.00246905      0.0      0.07040478      0.0
T008j_      11.0      0.0      0.0      180.0      0.0020738      0.0      0.07202368      0.0
T009a_      10.0      0.0      0.0      0.0      0.00172884      0.0      0.07348806      0.0
T009b_      9.0      0.0      0.0      180.0      0.00132268      0.0      0.0747993      0.0
T009c_      8.0      0.0      0.0      0.0      0.00101584      0.0      0.07596263      0.0
T009d_      7.0      0.0      0.0      0.0      0.00076636      0.0      0.07698116      0.0
T009e_      6.0      0.0      0.0      180.0      0.00056152      0.0      0.0778579      0.0
T009f_      5.0      0.0      0.0      0.0      0.00039515      0.0      0.07859522      0.0
T009g_      4.0      0.0      0.0      180.0      0.0003285      0.0      0.07919725      0.0
T009h_      3.0      0.0      0.0      0.0      0.00024078      0.0      0.07966424      0.0
T009i_      2.0      0.0      0.0      180.0      0.00015637      0.0      0.07999694      0.0
T009j_      1.0      0.0      0.0      180.0      7.686e-05      0.0      0.08019628      0.0
T010a_      0.0      0.0      0.0      180.0      0.0      0.0      0.08026266      0.0
T010b_      -1.0      0.0      0.0      180.0      7.686e-05      180.0      0.08019628      0.0
T010c_      -2.0      0.0      0.0      180.0      0.00015637      180.0      0.07999694      0.0
T010d_      -3.0      0.0      0.0      180.0      0.00024078      180.0      0.07966424      0.0
T010e_      -4.0      0.0      0.0      180.0      0.0003285      180.0      0.07919725      0.0
T010f_      -5.0      0.0      0.0      0.0      0.00039515      180.0      0.07859522      0.0
T010g_      -6.0      0.0      0.0      180.0      0.00056152      180.0      0.0778579      0.0
T010h_      -7.0      0.0      0.0      0.0      0.00076636      180.0      0.07698116      0.0
T010i_      -8.0      0.0      0.0      0.0      0.00101584      180.0      0.07596263      0.0
T010j_      -9.0      0.0      0.0      180.0      0.00132268      180.0      0.0747993      0.0
T011a_      -10.0      0.0      0.0      0.0      0.00172884      180.0      0.07348806      0.0
T011b_      -11.0      0.0      0.0      180.0      0.0020738      180.0      0.07202368      0.0
T011c_      -12.0      0.0      0.0      0.0      0.00246905      180.0      0.07040478      0.0
T011d_      -13.0      0.0      0.0      180.0      0.00291986      180.0      0.06862758      0.0
T011e_      -14.0      0.0      0.0      180.0      0.00343139      180.0      0.06668724      0.0
T011f_      -15.0      0.0      0.0      180.0      0.00400978      180.0      0.06457863      0.0
T011g_      -16.0      0.0      0.0      180.0      0.00466033      180.0      0.06229588      0.0
T011h_      -17.0      0.0      0.0      0.0      0.00538913      180.0      0.05983281      0.0
T011i_      -18.0      0.0      0.0      0.0      0.00620119      180.0      0.05718236      0.0
T011j_      -19.0      0.0      0.0      180.0      0.00710717      180.0      0.05433795      0.0
T012a_      -20.0      0.0      0.0      180.0      0.00811058      180.0      0.05129079      0.0
T012b_      -21.0      0.0      0.0      0.0      0.00921903      180.0      0.04803214      0.0
T012c_      -22.0      0.0      0.0      0.0      0.01044101      180.0      0.0445527      0.0
T012d_      -23.0      0.0      0.0      180.0      0.01178673      180.0      0.04084254      0.0
T012e_      -24.0      0.0      0.0      0.0      0.01326134      180.0      0.03688951      0.0
T012f_      -25.0      0.0      0.0      180.0      0.01487531      180.0      0.03268167      0.0
T012g_      -26.0      0.0      0.0      180.0      0.01663991      180.0      0.02820653      0.0
T012h_      -27.0      0.0      0.0      180.0      0.01856487      180.0      0.02344903      0.0
T012i_      -28.0      0.0      0.0      180.0      0.02066099      180.0      0.01839363      0.0
T012j_      -29.0      0.0      0.0      0.0      0.02294077      180.0      0.01302322      0.0
T013a_      -30.0      0.0      0.0      0.0      0.0254177      180.0      0.0073196      0.0
T013b_      -31.0      0.0      0.0      0.0      0.02810402      180.0      0.00126201      0.0
T013c_      -32.0      0.0      0.0      0.0      0.03101605      180.0      0.00517098      180.0
T013d_      -33.0      0.0      0.0      180.0      0.03416997      180.0      0.01200308      180.0
T013e_      -34.0      0.0      0.0      180.0      0.03758225      180.0      0.01926098      180.0
T013f_      -35.0      0.0      0.0      0.0      0.04127244      180.0      0.02697235      180.0
T013g_      -36.0      0.0      0.0      0.0      0.04526055      180.0      0.03516905      180.0
T013h_      -37.0      0.0      0.0      0.0      0.04956792      180.0      0.04388521      180.0
T013i_      -38.0      0.0      0.0      0.0      0.05422023      180.0      0.05315785      180.0
T013j_      -39.0      0.0      0.0      0.0      0.05924159      180.0      0.06302889      180.0
T014a_      -40.0      0.0      0.0      0.0      0.06466146      180.0      0.07354326      180.0
T014b_      -41.0      0.0      0.0      0.0      0.07051011      180.0      0.08475147      180.0
T014c_      -42.0      0.0      0.0      0.0      0.07682211      180.0      0.09670733      180.0
T014d_      -43.0      0.0      0.0      0.0      0.08363287      180.0      0.1094723      180.0
T014e_      -44.0      0.0      0.0      0.0      0.09098305      180.0      0.12311339      180.0
T014f_      -45.0      0.0      0.0      0.0      0.09891785      180.0      0.13770349      180.0
T014g_      -46.0      0.0      0.0      0.0      0.10748388      180.0      0.15332711      180.0
T014h_      -47.0      0.0      0.0      0.0      0.11673299      180.0      0.17007682      180.0
T014i_      -48.0      0.0      0.0      0.0      0.12672212      180.0      0.18805644      180.0
T014j_      -49.0      0.0      0.0      0.0      0.13751409      180.0      0.20738382      180.0
T015a_      -50.0      0.0      0.0      0.0      0.14917625      180.0      0.22819011      180.0
T015b_      -51.0      0.0      0.0      0.0      0.16178341      180.0      0.25062241      180.0
T015c_      -52.0      0.0      0.0      0.0      0.17541818      180.0      0.27484789      180.0
T015d_      -53.0      0.0      0.0      0.0      0.19017143      180.0      0.30105285      180.0
T015e_      -54.0      0.0      0.0      0.0      0.20614324      180.0      0.32944969      180.0
T015f_      -55.0      0.0      0.0      0.0      0.22344227      180.0      0.36027876      180.0
T015g_      -56.0      0.0      0.0      0.0      0.24218839      180.0      0.39381455      180.0
T015h_      -57.0      0.0      0.0      0.0      0.26251516      180.0      0.43037114      180.0
T015i_      -58.0      0.0      0.0      0.0      0.28456754      180.0      0.47030641      180.0
T015j_      -59.0      0.0      0.0      0.0      0.3085056      180.0      0.51403475      180.0
T016a_      -60.0      0.0      0.0      0.0      0.33450596      180.0      0.56203432      180.0
T016b_      -61.0      0.0      0.0      0.0      0.36276517      180.0      0.61486002      180.0
T016c_      -62.0      0.0      0.0      0.0      0.39350083      180.0      0.67315745      180.0
T016d_      -63.0      0.0      0.0      0.0      0.4269572      180.0      0.7376837      180.0
T016e_      -64.0      0.0      0.0      0.0      0.46340933      180.0      0.80933103      180.0
T016f_      -65.0      0.0      0.0      0.0      0.50316947      180.0      0.8891555      180.0
T016g_      -66.0      0.0      0.0      0.0      0.5465997      180.0      0.97841632      180.0
T016h_      -67.0      0.0      0.0      0.0      0.5941336      180.0      1.07863734      180.0
T016i_      -68.0      0.0      0.0      0.0      0.64631572      180.0      1.19168836      180.0
T016j_      -69.0      0.0      0.0      0.0      0.70385818      180.0      1.31990208      180.0
T017a_      -70.0      0.0      0.0      0.0      0.76772526      180.0      1.46624419      180.0
T017b_      -71.0      0.0      0.0      0.0      0.83937807      180.0      1.63479234      180.0
T017c_      -72.0      0.0      0.0      0.0      0.9206195      180.0      1.83040081      180.0
T017d_      -73.0      0.0      0.0      0.0      1.0144758      180.0      2.060454      180.0
T017e_      -74.0      0.0      0.0      0.0      1.12552647      180.0      2.33556681      180.0
T017f_      -75.0      0.0      0.0      0.0      1.26092326      180.0      2.67188056      180.0
T017g_      -76.0      0.0      0.0      0.0      1.43193202      180.0      3.09502669      180.0
T017h_      -77.0      0.0      0.0      0.0      1.65697048      180.0      3.64878038      180.0
T017i_      -78.0      0.0      0.0      0.0      1.96605092      180.0      4.41154788      180.0
T017j_      -79.0      0.0      0.0      180.0      2.40986623      180.0      5.54029398      180.0
T018a_      -80.0      0.0      0.0      180.0      3.07173383      180.0      7.44425099      180.0
T018k_      -80.1      0.0      0.0      180.0      3.1513316      180.0      7.74698789      180.0
T018l_      -80.2      0.0      0.0      180.0      3.22608252      180.0      8.07652316      180.0
T018m_      -80.3      0.0      0.0      180.0      3.29706045      180.0      8.4234121      180.0
T018n_      -80.4      0.0      0.0      180.0      3.36456842      180.0      8.78460748      180.0
T018o_      -80.5      0.0      0.0      180.0      3.4286163      180.0      9.15831286      180.0
T018p_      -80.6      0.0      0.0      180.0      3.48919603      180.0      9.54313463      180.0
T018q_      -80.7      0.0      0.0      180.0      3.54633349      180.0      9.93790638      180.0
T018r_      -80.8      0.0      0.0      180.0      3.60006884      180.0      10.3416563      180.0
T018s_      -80.9      0.0      0.0      180.0      3.65044429      180.0      10.75356225      180.0
T018b_      -81.0      0.0      0.0      180.0      3.69752498      180.0      11.17286116      180.0
T018c_      -82.0      0.0      0.0      180.0      3.99649211      180.0      15.65216998      180.0
T018d_      -83.0      0.0      0.0      180.0      4.00243053      180.0      20.36290613      180.0
T018e_      -84.0      0.0      0.0      180.0      3.70488552      180.0      24.95336643      180.0
T018f_      -85.0      0.0      0.0      180.0      3.03418257      180.0      28.89743912      180.0
T018g_      -86.0      0.0      0.0      180.0      2.22852769      180.0      31.03120073      180.0
T018h_      -87.0      0.0      0.0      180.0      1.56920606      180.0      32.29336617      180.0
T018i_      -88.0      0.0      0.0      180.0      1.0024368      180.0      33.06261597      180.0
T018j_      -89.0      0.0      0.0      180.0      0.48889566      180.0      33.48247301      180.0
T019a_      -89.99      0.0      0.0      180.0      0.00485373      180.0      33.61783197      180.0
