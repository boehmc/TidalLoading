# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# ## Poisson's Equation to Compute $\Phi$

# +
# %matplotlib inline
# %config Completer.use_jedi = False

import salvus.namespace as sn
from salvus.mesh import simple_mesh
from salvus.flow import api
import numpy as np
# -

# ## Setup the mesh

G = 6.67430e-11
M1 = 1 / (4 * np.pi * G)

# +
m = simple_mesh.Globe3D()
m.basic.elements_per_wavelength = 1.0
m.basic.model = "prem_iso_no_crust"
m.basic.min_period_in_seconds = 60.0
m.advanced.tensor_order = 2

m.gravity_mesh.add_exterior_domain = True
# print(m)
mesh = m.create_mesh()
print(mesh.elemental_fields.keys())

f = np.ones_like(mesh.elemental_fields["VP"])


del mesh.elemental_fields['VP']
del mesh.elemental_fields['VS']
del mesh.elemental_fields['RHO']
mesh.attach_field("rhs", 5513.0 * f)
mesh.elemental_fields["fluid"] = 1.0 * np.ones([mesh.nelem])

mesh.attach_field("M0", 1.0 * f)
mesh.attach_field("M1", M1 * f)
mask = mesh.elemental_scalar_fields['external'] == 1.0
mesh.element_nodal_fields['rhs'][mask] = 0.0
print(mesh.elemental_fields.keys())
mesh.write_h5('rhs.h5')

# +
w = sn.simple_config.simulation.Poisson(mesh=mesh)

w.domain.polynomial_order = mesh.shape_order

w.physics.poisson_equation.right_hand_side.filename = "rhs.h5"
w.physics.poisson_equation.right_hand_side.format = "hdf5"
w.physics.poisson_equation.right_hand_side.field = "rhs"

w.physics.poisson_equation.solution.filename = "solution.h5"

bc = sn.simple_config.boundary.HomogeneousDirichlet(side_sets=["r2"])
w.add_boundary_conditions(bc)

w.linear_solver.max_iterations = 1000
w.linear_solver.absolute_tolerance = 0.0
w.linear_solver.relative_tolerance = 1e-6

w.validate()
w
# -

# ## Run the simulation and visualize

# We use SalvusFlow to run the simulation. The site determines
# where it will run in the end. Might be the local machine, or
# a large remote cluster.
api.run(input_file=w, site_name="tides", output_folder="global_simulation", overwrite=True, ranks=4)

# !cat global_simulation/stdout

# !open global_simulation/solution.xdmf
# !open rhs.xdmf


