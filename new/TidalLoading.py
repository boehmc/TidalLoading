# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.2
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Tidal Loading

# You probably need to adjust this path depending on where you store the fields.
load_model_file = '../../../TidalLoading/data-models/load-models/tidal_elevation_filtered_TPX09_m2.nc'

# +
# %matplotlib inline
# %config Completer.use_jedi = False

import salvus.namespace as sn
from salvus.mesh import simple_mesh
from salvus.flow import api
import numpy as np
# -

# ## Constants and parameters

# +
G = 6.67430e-11
M0 = 1 / (4 * np.pi * G)

period = 200.0
order = 1
# -

# ## Generate mesh

# +
m = simple_mesh.TidalLoading()
m.basic.model = "prem_iso_one_crust"
m.basic.nex = 36
m.basic.tidal_loading_file = load_model_file
m.basic.mantle_refinement_index = 6
m.advanced.tensor_order = order
m.gravity_mesh.add_exterior_domain = True

mesh = m.create_mesh()
mesh.map_nodal_fields_to_element_nodal()
mesh.write_h5('tidal.h5')
# -

# !open tidal.xdmf

# ## Step 1: Collect Inputs for Tidal Loading
#
# Here we use the background $\Phi$ provided by the mesh.

# +
loading_mesh = mesh.copy()
f = np.zeros_like(loading_mesh.elemental_fields["VP"])

# spherical caps
earth_radius_in_meters = 6371000.0
radius_in_degrees = 10.0
height_in_meters = 1.0
density=1000.0

cap_radius_in_meters = np.sin(radius_in_degrees * np.pi / 180.0) * earth_radius_in_meters
p = loading_mesh.get_element_nodes()
rr = np.linalg.norm(p, axis=2)

# select points within the cap radius from the poles
mask = np.sqrt( p[:,:,0] ** 2 + p[:,:,1] ** 2)  < cap_radius_in_meters
f[mask] = 1.0

# only apply caps at the poles
mask = np.abs(rr - earth_radius_in_meters) > 1.0
f[mask] = 0.0
loading_mesh.attach_field("gamma", density * height_in_meters * f)
# -

loading_mesh.write_h5("load.h5")

# !open load.xdmf

# ## Step 2: Solve Loading Problem

# +
w = sn.simple_config.simulation.TidalLoading(mesh=loading_mesh)

w.domain.polynomial_order = mesh.shape_order

w.physics.tidal_loading.load_model.filename = "load.h5"
w.physics.tidal_loading.load_model.format = "hdf5"
w.physics.tidal_loading.load_model.field = "gamma"

w.physics.tidal_loading.solution.filename = "solution.h5"

bc = sn.simple_config.boundary.HomogeneousDirichlet(side_sets=["r2"])
w.add_boundary_conditions(bc)

w.linear_solver.max_iterations = 10
w.linear_solver.absolute_tolerance = 0.0
w.linear_solver.relative_tolerance = 1e-6

w
# -

api.run(input_file=w, 
        site_name="tides", 
        output_folder="loading", 
        overwrite=True, ranks=4)


