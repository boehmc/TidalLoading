# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.2
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Spherical Caps

# +
# %matplotlib inline
# %config Completer.use_jedi = False

import salvus.namespace as sn
from salvus.mesh import simple_mesh
from salvus.flow import api
import numpy as np
# -

# ## Constants and parameters

# +
G = 6.67430e-11
M0 = 1 / (4 * np.pi * G)

period = 200.0
order = 2
# -

# ## Generate mesh

# +
m = simple_mesh.TidalLoading()
m.basic.model = "prem_iso_one_crust"
m.basic.nex = 36
m.advanced.tensor_order = order
m.gravity_mesh.add_exterior_domain = True

mesh = m.create_mesh()
mesh.map_nodal_fields_to_element_nodal()
mesh.write_h5('tidal.h5')
# -

# !open tidal.xdmf

# ## Step 1: Precompute Gravitational Potenial $\Phi$ by Solving Poisson's Equation

gravity_mesh = mesh.copy()
gravity_mesh.write_h5("rhs.h5")
f = np.ones_like(gravity_mesh.elemental_fields["VP"])
gravity_mesh.elemental_fields = {}
gravity_mesh.elemental_fields["fluid"] = np.ones([mesh.nelem])
gravity_mesh.attach_field("M0", M0 * f)
gravity_mesh.attach_field("M1", 1.0 * f)


# +
w = sn.simple_config.simulation.Poisson(mesh=gravity_mesh)

w.domain.polynomial_order = gravity_mesh.shape_order

w.physics.poisson_equation.right_hand_side.filename = "rhs.h5"
w.physics.poisson_equation.right_hand_side.format = "hdf5"
w.physics.poisson_equation.right_hand_side.field = "RHO"

w.physics.poisson_equation.solution.filename = "Phi.h5"

bc = sn.simple_config.boundary.HomogeneousDirichlet(side_sets=["r2"])
w.add_boundary_conditions(bc)

w.linear_solver.max_iterations = 10
w.linear_solver.absolute_tolerance = 0.0
w.linear_solver.relative_tolerance = 1e-6

w.validate()
w
# -

api.run(input_file=w, 
        site_name="tides", 
        output_folder="gravity", 
        overwrite=True, ranks=1)

# ## Step 2: Define the load for the caps

# +
loading_mesh = mesh.copy()
f = np.zeros_like(loading_mesh.elemental_fields["VP"])
# loading_mesh.elemental_fields = {}
loading_mesh.elemental_fields["fluid"] = np.zeros([mesh.nelem])


# spherical caps
earth_radius_in_meters = 6371000.0
radius_in_degrees = 10.0
height_in_meters = 1.0
density=1000.0

cap_radius_in_meters = np.sin(radius_in_degrees * np.pi / 180.0) * earth_radius_in_meters
p = loading_mesh.get_element_nodes()
rr = np.linalg.norm(p, axis=2)

# select points within the cap radius from the poles
mask = np.sqrt( p[:,:,0] ** 2 + p[:,:,1] ** 2)  < cap_radius_in_meters
f[mask] = 1.0

# only apply caps at the poles
mask = np.abs(rr - earth_radius_in_meters) > 1.0
f[mask] = 0.0
loading_mesh.attach_field("gamma", density * height_in_meters * f)
# -

loading_mesh.write_h5("load.h5")

# !open load.xdmf

# ## Step 3: Solve Tidal Loading

# +
w = sn.simple_config.simulation.TidalLoading(mesh=loading_mesh)

w.domain.polynomial_order = mesh.shape_order

w.physics.tidal_loading.load_model.filename = "load.h5"
w.physics.tidal_loading.load_model.format = "hdf5"
w.physics.tidal_loading.load_model.field = "gamma"

w.physics.tidal_loading.solution.filename = "solution.h5"

bc = sn.simple_config.boundary.HomogeneousDirichlet(side_sets=["r2"])
w.add_boundary_conditions(bc)

w.linear_solver.max_iterations = 10
w.linear_solver.absolute_tolerance = 0.0
w.linear_solver.relative_tolerance = 1e-6

w
# -

api.run(input_file=w, 
        site_name="tides", 
        output_folder="loading", 
        overwrite=True, ranks=4)


