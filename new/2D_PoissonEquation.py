# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# ## 2D Poisson's Equation
#
#

# +
# %matplotlib inline
# %config Completer.use_jedi = False

import salvus.namespace as sn
from salvus.mesh import simple_mesh
from salvus.flow import api
import numpy as np
# -

# ## Setup the mesh

# +
m = simple_mesh.CartesianHomogeneousIsotropicElastic2D(vp=2.0, vs=1.0, rho=1.0,
                                                      x_max=2.0, y_max=2.0,
                                                      max_frequency=10.0)
m.advanced.tensor_order = 2
mesh = m.create_mesh()

f = np.ones_like(mesh.elemental_fields["VP"])
mesh.elemental_fields = {}
mesh.attach_field("M0", 1.0 * f)
mesh.attach_field("M1", 1.0 * f)

mesh.elemental_fields["fluid"] = 1.0 * np.ones([mesh.nelem])

mesh.attach_field("rhs", 1.0 * f)
mesh.write_h5('rhs_2d.h5')
mesh.write_h5('mesh.h5')
# -

mesh

# +
w = sn.simple_config.simulation.Poisson(mesh=mesh)

w.domain.polynomial_order = mesh.shape_order

w.physics.poisson_equation.right_hand_side.filename = "rhs_2d.h5"
w.physics.poisson_equation.right_hand_side.format = "hdf5"
w.physics.poisson_equation.right_hand_side.field = "rhs"

w.physics.poisson_equation.solution.filename = "solution.h5"

# w.output.volume_data.fields = ["phi"]
# w.output.volume_data.filename = "iterations.h5"
# w.output.volume_data.format = "hdf5"
# w.output.volume_data.sampling_interval_in_time_steps = 100

boundaries = sn.simple_config.boundary.HomogeneousDirichlet(
    side_sets=["x0", "x1", "y0", "y1"]
)

# Associate boundaries with our simulation.
w.add_boundary_conditions(boundaries)

w.linear_solver.max_iterations = 10000
w.linear_solver.absolute_tolerance = 0.0
w.linear_solver.relative_tolerance = 1e-10

w.validate()
# -

api.run(input_file=w, 
        site_name="tides", 
        output_folder="2d_simulation",
        overwrite=True, ranks=4)

# +
# inspect solution in Paraview
# # !open 2d_simulation/solution.xdmf
# -

# !cat 2d_simulation/stdout

# !open 2d_simulation/solution.xdmf


